const route = require ('express').Router()
const { page , user } = require("../contoller")
const middleware = require("../middleware")

route.get('/' , page.mainPage)
route.get('/play-game' , middleware.putMiddleware, page.suitGame)
route.get('/login' , page.loginPage)

route.get('/data-user' , user.getUser)
// route.get('/data-userID/:id' , user.getUserByID)

route.post('/login' , user.login)

module.exports = route